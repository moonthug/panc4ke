/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package

// Library


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param name
 * @constructor
 */
var Notification = function(name) {
    this.name = name;
    this.data = {};

    /**
     *
     * @type {Array}
     * @private
     */
    this._subscribers = [];
};



///////////////////////////////////////
//
// PUBLIC METHODS

/**
 *
 * @param {function} cb
 */
Notification.prototype.addSubscriber = function(cb) {
    this._subscribers.push(cb);
};

/**
 *
 * @param {Object} data
 */
Notification.prototype.notify = function(data) {
    var self = this;
    this._subscribers.forEach(function(subscriber) {
        subscriber({
            name: self.name,
            data: data
        });
    });
};


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Notification;