/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var util = require('util');

// Internal
var Controller = require('./controller');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var WSController = function(app) {
    WSController.super_.call(this, app);

    this.io             = null;
    this.socket         = null;
    this.sessionData    = {};

    this.commandCb      = null;
    this.namespace      = '';
    this.commands       = [];

    this.config = this.app.getConfig('websocket');
};


///////////////////////////////////////
//
// INHERITS

Controller.extend(WSController);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
WSController.extend = function(constructor) {
    util.inherits(constructor, WSController);
};


///////////////////////////////////
//
// PUBLIC METHODS


///////////////////////////////////
//
// PROTECTED METHODS

/**
 *
 * @param {string} command
 * @param {object} data
 * @param {string} id
 * @protected
 */
WSController.prototype.emit = function(command, data, id) {
    if(!data) {
        data = command;
        global.log.verbose('>>> [%s]', '', data);
        this.commandCb.call(this, null, data);
        this.commandCb = null;
    }
    else {
        command = this._createCommand(command, id);
        global.log.verbose('>>> [%s] ', command, data);
        this.socket.emit(command, data);
    }
};

/**
 *
 * @param {Error} error
 * @param {number} socketId
 * @protected
 */
WSController.prototype.emitError = function(error, socketId) {
    var command = 'error:update';
    var data = { message: error.toString() };
    global.log.error('>>> [%s] ', command, data);

    if(socketId) {
        var wsServer = this.io || this.app.getService('websocket').getWebsocketServer();
        wsServer.sockets.socket(socketId).emit(command, data);
    }
    else
        this.socket.emit(command, data);
};

/**
 *
 * @param {string} command
 * @param {object} data
 * @param {string} id
 * @protected
 */
WSController.prototype.broadcast = function(command, data, id) {
    command = this._createCommand(command, id);
    global.log.verbose('))) [%s] ', command, data);
    this.socket.broadcast.emit(command, data);
};

/**
 *
 * @param {Error} error
 * @protected
 */
WSController.prototype.broadcastError = function(error) {
    var wsServer = this.io || this.app.getService('websocket').getWebsocketServer();
    var command = 'error:update';
    var data = { message: error.toString() };
    global.log.verbose('))) [%s] ', command, data);
    wsServer.sockets.emit(command, data);
};

/**
 *
 * @param {string} command
 * @param {object} data
 * @param {string} id
 * @protected
 */
WSController.prototype.broadcastToAll = function(command, data, id) {
    var wsServer = this.io || this.app.getService('websocket').getWebsocketServer();
    command = this._createCommand(command, id);
    global.log.verbose('))) [%s] ', command, data);
    wsServer.sockets.emit(command, data);
};

/**
 *
 * @param {string} command
 * @param {object} data
 * @param {string} except
 * @param {string} id
 * @protected
 */
WSController.prototype.broadcastToAllExcept = function(command, data, except, id) {
    var wsServer = this.io || this.app.getService('websocket').getWebsocketServer();
    command = this._createCommand(command, id);
    global.log.verbose(')x) [%s] ', command, data);
    wsServer.sockets.except(except).emit(command, data);
};

/**
 *
 * @param {SocketIO} io
 * @param {Socket} socket
 * @protected
 */
WSController.prototype.bindToSocket = function(io, socket) {
    var self = this;

    this.io             = io;
    this.socket         = socket;
    this.sessionData    = socket.handshake.session;


    this.commands.forEach(function(c) {
        var command = self._createCommand(c.command);

        console.log('BIND COMMAND:', command);

        if(!c.method) throw new Error('No handler method specified for: ' + command);

        global.log.verbose('[WS] Bind to command: %s', command);
        socket.on(command, function(data, callback) {
            self.commandCb = callback;
            global.log.verbose('<<< [%s] ', command, data);
            c.method.call(self, data, callback);
        });
    });
};

/**
 *
 * @param command
 * @param method
 * @protected
 */
WSController.prototype.addCommand = function(command, method) {
    this.commands.push({
        command : command,
        method  : method
    });
};

/**
 *
 * @param {string} notification
 * @param {Object} data
 * @protected
 */
WSController.prototype.sendNotification = function(notification, data) {
    data.user = this.sessionData.user;
    data.socketId = this.sessionData.socketId;

    WSController.super_.prototype.sendNotification(notification, data);
};


///////////////////////////////////
//
// PRIVATE METHODS

/**
 *
 * @param {string} command
 * @param {string} id
 * @returns {string}
 * @private
 */
WSController.prototype._createCommand = function(command, id) {
    var ns = typeof id != 'undefined' ? this.namespace + '/' + id : this.namespace;
    return ns + ':' + command;
};


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = WSController;