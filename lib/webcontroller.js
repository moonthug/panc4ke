/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var util = require('util');

// Library

// Internal
var Controller = require('./controller');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var WebController = function(app) {
    WebController.super_.call(this, app);

    this.routes   = [];
    this.config   = this.app.getConfig('web');
};


///////////////////////////////////////
//
// INHERITS

Controller.extend(WebController);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
WebController.extend = function(constructor) {
    util.inherits(constructor, WebController);
};


///////////////////////////////////
//
// PUBLIC METHODS


///////////////////////////////////
//
// PROTECTED METHODS

/**
 *
 * @param {Router} router
 */
WebController.prototype.bindToExpress = function(express, server) {
    var self = this;

    var router = express.Router();

    this.routes.forEach(function(r) {
        var route = self._createRoute(r.route);
        global.log.verbose('[WEB] Bind to route: %s /%s%s', r.verb, self.namespace, route);
        router[r.verb](route, function(req, res, next) {
            r.method.call(self, req, res, next);
        });
    });

    server.use('/' + this.namespace, router);
};


/**
 *
 * @param {string} verb
 * @param {string} route
 * @param {function} method
 * @protected
 */
WebController.prototype.addRoute = function(verb, route, method) {
    this.routes.push({
        verb    : verb,
        route   : route,
        method  : method
    });
};


///////////////////////////////////
//
// PRIVATE METHODS

/**
 *
 * @param {string} route
 * @returns {string}
 * @private
 */
WebController.prototype._createRoute = function(route) {
    // Index route
    if(route == 'index') return '';

    // Everything else
    return '/' + route;
}

///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = WebController;