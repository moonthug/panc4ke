/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var util    = require('util');

// Library

// Internal
var Base = require('./base');

///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var Actor = function(app) {
    this.app = app;
};


///////////////////////////////////////
//
// INHERITS

Base.extend(Actor);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
Actor.extend = function(constructor) {
    util.inherits(constructor, Actor);
};


///////////////////////////////////////
//
// PUBLIC METHODS


///////////////////////////////////////
//
// PROTECTED METHODS

/**
 *
 * @param {string} notification
 * @param {Object} data
 * @protected
 */
Actor.prototype.sendNotification = function(notification, data) {
    this.app.sendNotification(notification, data);
};


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Actor;