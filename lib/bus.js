/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package

// Library

// Internal
var Notification = require('./notification');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @constructor
 */
var Bus = function() {
    this.notificationMap = {};
};


///////////////////////////////////
//
// PUBLIC METHODS

/**
 *
 * @param {string} notification
 * @param {Object=} data
 */
Bus.prototype.sendNotification = function(notification, data) {
    if(this.notificationMap[notification]) {
        this.notificationMap[notification].notify(data);
    }
};

/**
 *
 * @param {string} notification
 * @param {function} callback
 */
Bus.prototype.subscribe = function(notification, callback) {
    if(!this.notificationMap[notification]) {
        this.notificationMap[notification] = new Notification(notification);
    }
    this.notificationMap[notification].addSubscriber(callback);
};


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Bus;