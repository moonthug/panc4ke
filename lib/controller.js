/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var util    = require('util');

// Internal
var Actor   = require('./actor');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var Controller = function(app) {
    Controller.super_.call(this, app);
    this.namespace  = '';
};


///////////////////////////////////////
//
// INHERITS

Actor.extend(Controller);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
Controller.extend = function(constructor) {
    util.inherits(constructor, Controller);
};


///////////////////////////////////////
//
// PUBLIC METHODS


///////////////////////////////////////
//
// PROTECTED METHODS

/**
 *
 * @param {string} namespace
 * @private
 */
Controller.prototype.setNamespace = function(namespace) {
    this.namespace = namespace;
};

///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Controller;