/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var fs   = require('fs'),
    path = require('path'),
    util = require('util');


// Library
var async   = require('async'),
    readdir = require('recursive-readdir');

// Internal
var Base    = require('./base'),
    Bus     = require('./bus');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var Facade = function(app) {
    this._config         = null;
    this._db             = null;
    this._services       = {};
    this._models         = {};
    this._controllers    = {
        ws  : {},
        web : {}
    };

    this.__basepath      = '.';

    /**
     *
     * @type {Bus}
     * @protected
     */
    this._bus            = new Bus();
};


///////////////////////////////////////
//
// INHERITS

Base.extend(Facade);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
Facade.extend = function(constructor) {
    util.inherits(constructor, Facade);
};


///////////////////////////////////////
//
// PUBLIC METHODS

/**
 *
 * @param {string} notificationName
 * @param {string=} commandName
 * @private
 */
Facade.prototype.registerCommand = function(notificationName, commandName) {
    commandName = commandName || notificationName;
    var command = this.loadActor('command', commandName, true);
    var commandContext = { app: this }
    this.subscribe(notificationName, command.bind(commandContext));
};

/**
 *
 * @param {string} notification
 * @param {Object=} data
 */
Facade.prototype.sendNotification = function(notification, data) {
    global.log.bus('Send Notification: %s', notification, (data ? data : null));
    this._bus.sendNotification(notification, data);
};

/**
 *
 * @param {string} notification
 * @param {function} callback
 */
Facade.prototype.subscribe = function(notification, callback) {
    this._bus.subscribe(notification, callback);
};

/**
 *
 * @param section
 * @param item
 * @returns {*}
 */
Facade.prototype.getConfig = function(section, item) {
    var config = this._config;

    if(section) {
        if(config.hasOwnProperty(section))
            config = config[section];
        else
            return null;
    }

    if(item) {
        if(config.hasOwnProperty(item))
            config = config[item];
        else
            return null;
    }

    return config;
};

/**
 *
 * @param {string} name
 * @returns {Service}
 */
Facade.prototype.getService = function(name) {
    if(!this._services[name])
        return global.log.error('The service "%s" doesn\'t exist...', name);

    return this._services[name];
};

/**
 *
 * @param {string} type
 * @param {string} name
 * @returns {Controller}
 */
Facade.prototype.getController = function(type, name) {
    if(!this._controllers[type] || !this._controllers[type][name])
        return global.log.error('The controller "%s/%s" doesn\'t exist...', type, name);

    return this._controllers[type][name];
};

/**
 *
 * @param {string} type
 * @param {string} name
 * @param {boolean} returnUninitialised
 * @returns {Actor||{}}
 */
Facade.prototype.loadActor = function(type, name, returnUninitialised) {
    var filePath = path.join(this.__basepath, type, name + '.js');

    if(!fs.existsSync(filePath))
        throw new Error('Could not load ' + type +': \'' + name + '\'');

    var actor = require(filePath);

    if(returnUninitialised != true) {
        actor = new actor(this);
    }

    if(type != 'command') {
        this._setActor(type, name, actor);
    }

    return actor;
};

/**
 *
 * @param io
 * @param socket
 */
Facade.prototype.bindControllersToIO = function(io, socket) {
    var self = this;
    Object.keys(this._controllers.ws).forEach(function(name) {
        var controller = self.getController('ws', name);
        controller.bindToSocket(io, socket);
    });
};

/**
 *
 * @param {Express} express
 * @param {Server} server
 */
Facade.prototype.bindControllersToExpress = function(express, server) {
    var self = this;
    Object.keys(this._controllers.web).forEach(function(name) {
        var controller = self.getController('web', name);
        controller.bindToExpress(express, server);
    });
};

///////////////////////////////////////
//
// PROTECTED METHODS

/**
 *
 * @param {Object} config
 * @protected
 */
Facade.prototype.init = function(config) {
    var self = this;

    this._config    = config;
    this.__basepath = config.app.basepath;

    async.series(
        [
            this._loadActorsFromBasedir.bind(this)
        ],
        function(err) {
            if(err) throw err;

            self.handleApplicationInitialised();
        }
    );
};


///////////////////////////////////////
//
// ABSTRACT PROTECTED METHODS

/**
 *
 * @private
 */
Facade.prototype.handleApplicationInitialised = function() {
    throw new Error('\'handleApplicationInitialised\' needs to be overridden');
};


///////////////////////////////////////
//
// PRIVATE METHODS

/**
 *
 * @param {function} cb
 * @private
 */
Facade.prototype._loadActorsFromBasedir = function(cb) {
    var self = this;
    async.forEach(
        ['controller', 'model', 'service'],
        function(type, next) {
            var typeBasePath = path.join(self.__basepath, type);
            readdir(typeBasePath, function(err, files) {
                if(err) return cb(err);

                files.forEach(function(file) {
                    if(path.extname(file) == '.js') {

                        var typeSubdir = path.dirname(path.relative(typeBasePath, file)),
                            currentType = type;

                        if(typeSubdir != '.') {
                            currentType += '/' + typeSubdir;
                        }

                        var actorName = path.basename(file, '.js');
                        self.loadActor(currentType, actorName);
                    }
                });

                next();
            });
        },
        cb
    );
};

/**
 *
 * @param {string} type
 * @param {string} name
 * @param {Actor} instance
 * @private
 */
Facade.prototype._setActor = function(type, name, instance) {
    var map = {};
    switch(type) {
        case 'controller/ws':
            map = this._controllers.ws;
            break;
        case 'controller/web':
            map = this._controllers.web;
            break;
        case 'model':
            map = this._models;
            break;
        case 'service':
            map = this._services;
            break;
        default:
            throw new Error('Invalid actor type specified: ' + type);
    }
    map[name] = instance;
};

/**
 *
 * @param {string} type
 * @param {string} name
 * @returns {Actor}
 * @private
 */
Facade.prototype._getActor = function(type, name) {
    return this[type][name];
};


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Facade;