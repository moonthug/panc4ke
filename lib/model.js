/**
 * Created by moonman
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Package
var util    = require('util');

// Internal
var Actor   = require('./actor');


///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

/**
 *
 * @param {Application} app
 * @constructor
 */
var Model = function(app) {
    Model.super_.call(this, app);
};


///////////////////////////////////////
//
// INHERITS

Actor.extend(Model);


///////////////////////////////////////
//
// STATIC METHODS

/**
 *
 * @param {function} constructor
 */
Model.extend = function(constructor) {
    util.inherits(constructor, Model);
};


///////////////////////////////////
//
// PUBLIC METHODS


///////////////////////////////////
//
// PROTECTED METHODS


///////////////////////////////////////////////////////////////////////////////
//
//
// EXPORTS
//

module.exports = Model;